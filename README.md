# Fichier de configuration

On utilise le fichier *settings.json* pour configurer l'application.
Un exemple de fichier :

```json
{
    // Lancer l'application en mode debug ?
    "debug": true,

    // Dossier dans lequel les projets à évaluer seront stockés.
    "projects_root": "Projets",

    // Fichier csv dans lequel les utilisateurs autorisés à se connecter seront stockés.
    // Le fichier doit contenir les colonnes 'user', 'password' et 'trial' correspondant à, respectivement,
    // le nom d'utilisateur (le groupe de projet), le mot de passe et le nombre de tentatives autorisées.
    "users_file": "users.csv",

    // Fichier csv dans lequel sont stockées les données de test.
    // Le fichier doit contenir les colonnes 'x' et 'expected', 'x' étant le chemin vers le fichier à classer et
    // 'expected' la classe du fichier.
    "test_data_file": "expected.csv",

    // Interval en secondes entre deux rafraichissement de données.
    "refresh_rate": 5,

    // Paramètres supplémentaires pour le mode debug.
    "debug_settings": {
        // Taille du jeu de test en mode debug.
        "row_count": 10
    }
}
```

# Utilisation

L'interface propose deux onglets : un onglet utilisateur, nécessitant une authentification, et un onglet résumant les résultats de l'ensemble des groupes.

## Evaluation d'un étudiant 

L'étudiant commence par s'authentifier. Il a ensuite accès a une page lui montrant les résultats obtenus à l'issu de sa précédente tentative.
Un formulaire lui propose de soumettre son projet, sous la forme d'un fichier zip. Un fichier `main.R` doit exister à la racine de l'archive soumise, le cas échéant, le projet ne sera pas évalué. L'archive est alors décompressée dans le dossier `project_root/user/tn` avec `n` le numéro de la tentative. Le fichier `project_root/user/tn/main.R` est chargé puis la fonction `classer` est appelé avec tous les données de test spécifiées dans `test_data_file`

## Affichage des résultats globaux

L'onglet *Graphiques* affiche les résultats moyen de tous les étudiant ayant déjà soumis un projet.

# Packages requis

Il faudra au préalable avoir installé :

- shiny
- dplyr
- readr
- magrittr
- glue
- jsonlite

```r
install.packages(c("shiny", "dplyr", "readr", "magrittr", "glue", "jsonlite"))
```