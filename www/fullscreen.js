function get_request_fullscreen() {
    var root = document.documentElement;
    return root.requestFullscreen || root.webkitRequestFullscreen || root.mozRequestFullScreen || root.msRequestFullscreen;
}

function get_exit_fullscreen() {
    return document.exitFullscreen || document.mozCancelFullScreen || document.webkitExitFullscreen || document.msExitFullscreen;
}

function is_fullscreen() {
    return document.fullscreenElement||document.webkitFullscreenElement ||document.mozFullScreenElement ||document.msFullscreenElement;
}

var request_fullscreen = get_request_fullscreen();
var exit_fullscreen = get_exit_fullscreen();


function update_screen_resolution(width, height) {
    Shiny.onInputChange('screen_width', width);
    Shiny.onInputChange('screen_height', height);
}


$(document).on('shiny:connected', function(e) {
    screen_width = 'auto';
    screen_height = 'auto';
    update_screen_resolution(screen_width, screen_height);


    $('#all_plot').dblclick(function(e) {
        if (is_fullscreen.call(document)) {
            exit_fullscreen.call(document);
            screen_width = 'auto';
            screen_height = 'auto';
        }
        else {
            request_fullscreen.call(e.target);
            screen_width = screen.width;
            screen_height = screen.height;
        }

        
        Shiny.onInputChange('rnd', Math.random());
        update_screen_resolution(screen_width, screen_height);
    })

    $(document).keyup(function(event) {
        if (event.keyCode == 13) {
            var focus_ok = $('#username').is(':focus') || $('#user_password').is(':focus');
            if (focus_ok) {
                $('#connect_button').click();
            }
        }
    });
});